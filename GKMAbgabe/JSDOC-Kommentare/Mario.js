'use strict';
/**
 * Spielbarer Charakter wird bestimmt
 * @public {int} jumpHeight Gibt an wie hoch die Spielfigur springt!
 * @public {int} size - Gibt die größe der Spielfigur an!
 * @public {int} box - Gibt an wo im Canvas der neue Spieler gespawnt wird!
 * @public {int} velocityY - VelocityY wird intialisiert und ist dafür zuständig wie schnell sich der PSieler bewegt
 * @public {function} jump - Der Sprung wrd ausgeführt und die Geschwindigkeit des Spielers wird angepasst!
 */
function Mario() {
	var jumpHeight = 200;
	var size = 50;
	this.box = new Box(50, GROUNDLEVEL, size, size);
	var velocityY = 0;
	this.jump = function () {
		console.log("jump");
		velocityY = -2;
	}
    /**
     * @public {function} draw - Das Bild des Spielers wird in das Objekt Spieler gepackt!
     * @public {int} box.y - Dem Bild der Spielfigur werden die gleichen Parameter wie der Box gegeben!
     * Wenn die Spielfigur eine gewisse Höhe erreicht hat ändert sich die Geschwindigkeit
     * @public {function} - Endgültige Zuweisung vom Bild
     */
	this.draw = function () {
		this.box.y = this.box.y + velocityY;
		if (this.box.y < jumpHeight)
			velocityY = 4;
		if (this.box.y >= GROUNDLEVEL)
			velocityY = 0;
		image(images.mario, this.box.left(), this.box.top(), size, size);
	}
}
