'use strict';
var highscore = 0;
var images;
var mario;
var bg;
var obstacles = [];
var GROUNDLEVEL = 450;
function preload() {
	var imgBackground = loadImage("assets/background.jpg"); //Geänderter Hintergrund
	var imgMario = loadImage("assets/mario.png"); //Geänderte Spielfigur
	var imgObstacle = loadImage("assets/obstacle.jpg");
	images = {
		background: imgBackground,
		mario: imgMario,
		obstacle: imgObstacle
	};
	console.log("end of preload");
}
function setup() {
	createCanvas(800, 500);
	mario = new Mario();
	bg = new Background();
	for(var i=0; i<5; i++)
		obstacles.push(new Obstacle());

	console.log("end of setup");
}
function draw() {
	bg.draw();
	for(var i=0; i<obstacles.length; i++){
		obstacles[i].draw();
		highscore = 0 + i;
		if (obstacles[i].box.isCollission(mario.box)) {
			console.log("collission: " + mario.box.toString() + ", " + obstacles[i].box.toString());
			noLoop();
		}
		fill(255);
		text("Highscore = " + i, 25, 25);
	}
	mario.draw();
}
function keyPressed() {
	mario.jump();
}
function mousePressed() {
	loop();
}